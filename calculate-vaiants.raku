my %total;
my $cps = "svgs".IO;
for $cps.dir {
	my $cp =  .Str.split("/")[*-1].parse-base(16).chr;
	#say $cp.uniprop('Block');
	%total{$cp.uniprop('Block')}{"count"}++;
	%total{$cp.uniprop('Block')}{"variants"}+=.dir;
}
say "| Block | Assigned | Total Variants |";
say "|-|-|-|";
for %total.keys.sort {
	printf "| %s | %s | %s |\n", 
		$_,
		%total{$_}{"count"}, 
		%total{$_}{"variants"};
}


#say "DONE";
