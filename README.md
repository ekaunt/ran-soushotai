# 乱草書体 － ran-soushotai

A cursive Japanese font that picks a random variant of each character you type.

The font currently has a total of 3,926 unique characters and 578,576 total glyphs/variants!

You can download the font [here](https://gitlab.com/ekaunt/ran-soushotai/-/raw/master/ran-soushotai.otf).

If you're running macOS then your milage may vary (contributions are welcome).

## Font Example

![font-example](./font-example.png)

## Coverage

| Block                                                  | Assigned / Total   | Total Variants   |
| :----------------------------------------------------- | -----------------: | ---------------: |
| Hiragana (U+3040-U+309F)                               | 88 / 93            | 430,935          |
| CJK Unified Ideographs Extension A (U+3400-U+4DBF)     | 6 / 6,582          | 44               |
| CJK Unified Ideographs (U+4E00-U+9FFF)                 | 3,812 / 20,976     | 140,347          |
| CJK Compatibility Ideographs (U+F900-U+FAFF)           | 7 / 472            | 20               |
| Kana Supplement (U+1B000-U+1B0FF)                      | 2 / 256            | 397              |
| Kana Extended-A                                        | 1 / 48             | 8                |
| Small Kana Extension (U+1B130-U+1B16F)                 | 3 / 7              | 6809             |
| CJK Unified Ideographs Extension B (U+20000-U+2A6DF)   | 7 / 42,711         | 16               |
|                                                        |                    |                  |
| Unicode coverage = 3926/137592 = 2.85%                 |                    | 578,576          |


## Font Variants

In the folder `extra-fonts` there's a couple of extra fonts that you may or may not find useful.

If you're unsure which font to use, then install ran-soushotai.otf in the main folder.

 - ran-soushotai-complete.otc
   - an opentype collection font that includes all 578,576 glyphs!
   - opentype collections are not very well supported, which is why its not the main font in this repository.
 - ran-soushotai-hiragana.otf
   - includes just hiragana, and is capped at 65,530 glyphs for maximum compatibility
 - ran-soushotai-hiragana-complete.otc
   - includes just hiragana, but has all 437,744 hiragana glyphs!
 - ran-soushotai-kanji.otf
   - includes just kanji, and is capped at 65,530 glyphs for maximum compatibility
 - ran-soushotai-kanji-complete.otc
   - includes just kanji, but has all 437,744 hiragana glyphs!

## Generation

What i used to generate the kanji svgs from this dataset: 

```sh
mkdir ran-soushotai
cd ran-soushotai
wget http://codh.rois.ac.jp/kmnist/dataset/kkanji/kkanji.tar
tar xf kkanji.tar
cd "kkanji2/";
dir="../kanji-svgs";
mkdir "$dir"
for char in U+*; do
        echo ${char#*+};
        mkdir ${dir}/${char#*+}
        i=1
        amount=$(ls $char | wc -l)
        for variant in $char/*; do
                echo -ne "\r$i/$amount"
                variant=${variant%.*} #remove the extension
                convert $variant.png -negate PGM:- |
                mkbitmap - -o - -t 0.45 |
                potrace - -s -o ${dir}/${char#*+}/${char#*+}.$((i++)).svg -t5 -a2 -n
                \rm $variant.pgm $variant.pbm
        done;echo
done;
cd ..
```

All generated kanji svg files are in the `svgs/kanji` folder so you can skip this step.

And similarly for the hiragana set:

```sh
git clone https://github.com/rois-codh/kmnist.git
cd kmnist
python ../extract-hiragana.py
cd hiragana;
dir="../../hiragana-svgs";
mkdir $dir;
for char in U+*; do
    echo ${char#*+};
    mkdir ${dir}/${char#*+}
    i=1
    amount=$(ls $char | wc -l)
    for variant in $char/*; do
        echo -ne "\r$((++i))/$amount"
        variant=${variant%.*} #remove the extension
        mkbitmap $variant.pgm --invert -o - -t 0.45 |
        potrace - -s -o ${dir}/${char#*+}/${char}.${i}.svg -t5 -a2 -n
    done;echo
done; 
cd ../..
```

All generated hiragana pgm files are in the file hiragana-source.tar, and all generated hiragana svg files are in the `svgs/hiragana` folder so you can skip this step.

Now you can run `font-generator.py` to generate the regular otf:

```sh
python font-generator.py --otf --name "ran-soushotai" kanji-svgs hiragana-svgs
```

Or to generate an opentype collection font run:

```sh
python font-generator.py --otc --name "ran-soushotai" kanji-svgs hiragana-svgs
otf2otc -o ran-soushotai.otc ran-soushotai?*.otf
```

You can also generate all font types simultaneously by running:

```sh
python font-generator.py --otc --otf --full-sfd --sfd --name "ran-soushotai" kanji-svgs hiragana-svgs
```

## License

The dataset to generate this font was taken from the [KMNIST dataset](https://github.com/rois-codh/kmnist), so it inherited their [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) (Attribution-ShareAlike 4.0 International 🅭🅯🄎) license.

"KMNIST Dataset" (created by CODH), adapted from "Kuzushiji Dataset" (created by NIJL and others), doi:10.20676/00000341
