# to extract all hiragana images from the dataset, since kmnist didnt do it like
# they did for kkanji2.tar
# it must be run in the kmnist directory.

# then run ↓ (after running this python script) to convert the images to svgs
# cd hiragana; dir="../hiragana-svgs";mkdir $dir;
# for char in U+*; do
#     echo ${char#*+};
#     mkdir ${dir}/${char#*+}
#     i=1
#     amount=$(ls $char | wc -l)
#     for variant in $char/*; do
#         echo -ne "\r$((++i))/$amount"
#         variant=${variant%.*} #remove the extension
#         mkbitmap $variant.pgm --invert -o - -t 0.45 |
#         potrace - -s -o ${dir}/${char#*+}/${char}.${i}.svg -t5 -a2 -n
#     done;echo
# done; echo $dir; cd ..


import numpy as np
import csv, os, urllib.request
from PIL import Image

imgs = np.load("k49-train-imgs.npz")['arr_0']
lbls = np.load("k49-train-labels.npz")['arr_0']

url="http://codh.rois.ac.jp/kmnist/dataset/k49/k49_classmap.csv"
path = url.split('/')[-1]
urllib.request.urlretrieve(url, path)
kana={}
with open(path, mode="r") as csvfile:
    csv_reader = csv.DictReader(csvfile)
    for row in csv_reader:
        kana[row["index"]] = row["codepoint"]#.split("+")[-1] # remove 'U+'


# map small and dakuten versions to each kana
kana_variants = { 
        "U+3042": ["U+3041"],
        "U+3044": ["U+3043"],
        "U+3046": ["U+3045","U+3094"],
        "U+3048": ["U+3047"],
        "U+304A": ["U+3049"],
        "U+304B": ["U+304C","U+3095"],
        "U+304D": ["U+304E"],
        "U+304F": ["U+3050"],
        "U+3051": ["U+3052", "U+3096"],
        "U+3053": ["U+3054"],
        "U+3055": ["U+3056"],
        "U+3057": ["U+3058"],
        "U+3059": ["U+305A"],
        "U+305B": ["U+305C"],
        "U+305D": ["U+305E"],
        "U+305F": ["U+3060"],
        "U+3061": ["U+3062"],
        "U+3064": ["U+3063", "U+3065"],
        "U+3066": ["U+3067"],
        "U+3068": ["U+3069"],
        "U+306F": ["U+3070", "U+3071"],
        "U+3072": ["U+3073", "U+3074"],
        "U+3075": ["U+3076", "U+3077"],
        "U+3078": ["U+3079", "U+307A"],
        "U+307B": ["U+307C", "U+307D"],
        "U+3084": ["U+3083"],
        "U+3086": ["U+3085"],
        "U+3088": ["U+3087"],
        "U+308F": ["U+308E"],
        "U+3090": ["U+1B150"],
        "U+3091": ["U+1B151"],
        "U+3092": ["U+1B152"],
        "U+309D": ["U+309E"],
        }


if not os.path.exists("hiragana"):
    os.makedirs("hiragana")
i=0
for image,label in zip(imgs, lbls):
    i+=1
    cp = kana[str(label)]

    # keep track of how many variants each kana has (raku: `kana{kana{cp}}+=1`)
    if not cp in kana:
        kana[cp] = 1
    else:
        kana[cp] += 1

    im = Image.fromarray(image)
    # im.show()

    # also export the images as all the kana variants 
    # (ie instead of just つ, also make っ and づ)
    cps = []
    if cp in kana_variants:
        cps = [[cp], kana_variants[cp]]
    else:
        cps = [[cp]]
    for cpt in [item for sublist in cps for item in sublist]:
        if not os.path.exists("hiragana/"+cpt):
            os.makedirs("hiragana/"+cpt)
        im.save("hiragana/"+cpt+"/"+cpt.split("+")[-1]+"."+str(kana[cp])+".pgm")

# print(kana)
print("total:",str(i))
