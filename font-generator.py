#!/usr/bin/env python

"""Font Generator

Usage:
    font-generator.py [--otc] [--full-sfd] [--otf] [--sfd] --name=<name> DIRS...
    font-generator.py (-h | --help)

Options:
  -f --otf̲   		create an OTF with exactly 65,530 total glyphs
			this is for the maximum compatibility
  -c --otc̲   		create multiple OTF files that you can join into an OTC
			this is if you have more than 65k glyphs, and want all
                        of them in your font. OTC isnt well supported though
  -s --s̲fd   		create an SFD (that you can play with in FontForge)
                        alongside the OTF or OTC
  -l --ful̲l-sfd  	create an SFD of the complete font with all glyphs 
  -n --n̲ame <name>  	the name the font should get (without an extension)
  DIRS    		the directory with all the subfolders that each have svg files.
			more than one folder is allowed
  -h --h̲elp  		Display this help message

Example:
    font-generator.py --otf --name "my font" folder1 folder2 folder3
"""
from docopt import docopt
from natsort import natsorted
import os, fontforge

args = docopt(__doc__)
# leave the program with the help message if none of the output option were specified
# what i want is: otc,full-sfd, and otf are all optional, but you must specify at
# least one, and if you do, then you 𝒄𝒂𝒏 specify --sfd to generate an sfd too
if (args['--otc'] or args['--full-sfd'] or args['--otf']):
    pass
elif args['--sfd']:
    print(__doc__)
    exit(1)
else:
    print(__doc__)
    exit(1)

font_name=args['--name']

lookup_num=0
font_number=0
def restart_lookup_table(font,reset=False):
    global lookup_num
    lookup_num = 1 if reset else lookup_num+1
    # add the 'rand' lookup
    font.addLookup("random"+str(lookup_num), "gsub_alternate", None, (("rand",(("DFLT",("dflt")),)),))
    font.addLookupSubtable("random"+str(lookup_num), "rand-sub"+str(lookup_num))

def generate_font(split):
    global font_number
    if split: font_number += 1
    # create and name the new font
    font = fontforge.font()
    font.familyname = font_name
    font.fontname = font_name+ ("-"+str(font_number) if split else "")
    font.fullname = font_name+ ("-"+str(font_number) if split else "")
    font.reencode("UnicodeFull")
    font.copyright = "Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)"
    restart_lookup_table(font,reset=True)
    return font

def export_font(font,split=True,ext="",count=""):
    fn = font_number if split else ""
    print("preparing font",font_name+str(fn),
            ("with "+str(count)+" glyphs") if count else "")
    font.selection.all()
    font.reencode("UnicodeFull")
    font.addExtrema()
    # print("validating",font_name+str(fn))
    # font.validate()
    if ext == "otf" or ext == "":
        print("generating font",font_name+str(fn)+".otf")
        font.generate(font_name+str(fn)+".otf")
    if ext == "sfd" or ext == "":
        print("saving font",font_name+str(fn)+".sfd")
        font.save(font_name+str(fn)+".sfd")
    # free up memory. 𝒗𝒆𝒓𝒚 𝒊𝒎𝒑𝒐𝒓𝒕𝒂𝒏𝒕
    font.close()


# go through all directories and get all the glyphs
print("gathering files")
folders = []
for folder in args['DIRS']:
    print(folder)
    folders.append(os.scandir(folder))
folders = [item for sublist in folders for item in sublist]
char_list = []
for i,char in enumerate(folders):
    glyphs = []
    for i,glyph in enumerate(os.scandir(char)):
        glyphs.append(glyph.path)
    char_list.append(natsorted(glyphs))

def add_chars(font, glyphs):
        glyph = glyphs.pop(0)
        character = font.createChar(glyph["cp"], glyph["name"])
        character.importOutlines(glyph["glyph"])
        # set the advance width to the fonts char height
        character.width = 1024 #font.em

        # add the 'rand' lookups to each character, including the base character
        variants = [character.glyphname]
        for i,glyph in enumerate(glyphs):
            #add the slots for the svg files in the font
            sub = font.createChar(-1, glyph["name"])#puts codepoint.number
            sub.importOutlines(glyph["glyph"])
            sub.width = 1024
            variants.append(sub.glyphname)
        character.addPosSub("rand-sub"+str(lookup_num), variants)


def char_walk():
    length = len(char_list) # to print a counter
    char_count = 0 # to not go over the max_chars limit
    print("progress"+"\t"+"cp"+"\t"+"variants"+"\t"+"total variants")
    for i,char in enumerate(char_list):
        if "glyphs" in locals():
            yield [glyphs, len(char)]
        # create the base glyph for each character
        cp = char[0].split("/")[-2]
        variant = 1
        #print where we're up to
        print("%8s"%(str(i+1)+"/"+str(length))+"\t"+cp+"\t"+"%8s"%(str(len(char)))+"\t"+str(len(char)+char_count))
        char_count += 1

        glyphs = [{"cp":int(cp.split(".")[0],16), "name":cp+"."+str(variant), "glyph":char[0]}]
        for i,glyph in enumerate(char):
            if i == 0: continue
            char_count += 1
            variant += 1
            cp = glyph.split("/")[-2]
            glyphs.append({"name":cp+"."+str(variant),"glyph": glyph})

    yield [glyphs, len(char)]
    print("Total glyphs: "+str(char_count)) #print where we're up to


def split_otf(sfd=False):
    font = generate_font(split=True)
    # the real limit is 65535, but im giving it a buffer just in case something
    max_chars = 65530
    char_count = 0 # to not go over the max_chars limit
    chars = char_walk()
    for glyphs, nxt in chars:
        char_count += len(glyphs)
        add_chars(font, glyphs)
        # split the lookup table every 5,000 glyphs cuz fontforge cant deal with large tables…
        if int((char_count+nxt)/5000) >= lookup_num:
            print("splitting lookup table", str(lookup_num)+"×")
            restart_lookup_table(font)
        if char_count + nxt > max_chars:
            export_font(font,count=char_count,split=True,ext=("" if sfd else "otf"))
            char_count = 0
            font = generate_font(split=True)

    if char_count > 0:
        export_font(font,count=char_count,split=True,ext=("" if sfd else "otf"))
    print("Now run `otf2otc -o font.otc font1.otf font2.otf...` to convert"+
            " all the separate font files into a single opentype-collection")


def full_sfd():
    font = generate_font(split=False)
    chars = char_walk()
    for glyphs, nxt in chars:
        add_chars(font, glyphs)
    export_font(font,split=False,ext="sfd")


def trimmed_otf(sfd=False):
    # the real limit is 65535, but im giving it a buffer just in case something
    max_chars = 65530
    global char_list
    print("Max amount of variants in a single char is", str(len(max(char_list, key=len))))
    print("Trimming till 65530 (because thats the maximum amount of chars SFNT allows)")
    while sum(len(x) for x in char_list) > max_chars:
        max(char_list, key=len).pop()
    char_list = [x for x in char_list if x]
    print("Trimmed until the max amount of variants in a single char is", str(len(max(char_list, key=len))))

    font = generate_font(split=False)
    char_count = 0 # to not go over the max_chars limit
    chars = char_walk()
    for glyphs, nxt in chars:
        char_count += len(glyphs)
        add_chars(font, glyphs)
        # split the lookup table every 5,000 glyphs cuz fontforge cant deal with large tables…
        if int((char_count+nxt)/5000) >= lookup_num:
            print("splitting lookup table", str(lookup_num)+"×")
            restart_lookup_table(font)

    export_font(font,split=False,ext=("" if sfd else "sfd"))


####################################################

if args['--full-sfd']:
    full_sfd()
if args['--otf']:
    trimmed_otf(args['--sfd'])
if args['--otc']:
    split_otf(args['--sfd'])


print("DONE!")
